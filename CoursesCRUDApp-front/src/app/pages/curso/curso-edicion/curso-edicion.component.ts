import { CursoService } from './../../../_service/curso.service';
import { Curso } from './../../../_model/curso';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-curso-edicion',
  templateUrl: './curso-edicion.component.html',
  styleUrls: ['./curso-edicion.component.css']
})
export class CursoEdicionComponent implements OnInit {

  id: number;
  curso: Curso;
  form: FormGroup;
  edicion: boolean = false;

  constructor(private cursoService: CursoService, private route: ActivatedRoute, private router: Router) {
    this.form = new FormGroup({
      'idcurso': new FormControl(0),
      'nombre': new FormControl(''),
      'descripcion': new FormControl(''),
      'horas': new FormControl(0),
      'docente': new FormControl(''),
      'vigencia': new FormControl()
    })
   }

  ngOnInit() {
    this.curso = new Curso();
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = this.id != null;
      this.initForm();
    });
  }

  initForm() {
    if (this.edicion) {
      this.cursoService.listarPorId(this.id).subscribe(data => {

        this.form = new FormGroup({
          'idcurso': new FormControl(this.id),
          'nombre': new FormControl(data.nombre),
          'descripcion': new FormControl(data.descripcion),
          'horas': new FormControl(data.cantHoras),
          'docente': new FormControl(data.docente),
          'vigencia': new FormControl(data.vigente)
        });
      });
    }
  }

  operar(){
    this.curso.idCurso = this.id;
    this.curso.nombre = this.form.value['nombre'];
    this.curso.descripcion = this.form.value['descripcion'];
    this.curso.cantHoras = this.form.value['horas'];
    this.curso.docente = this.form.value['docente'];
    this.curso.vigente = this.form.value['vigencia'];

    if(this.curso != null && this.curso.idCurso > 0){
      //update
      this.cursoService.modificar(this.curso).subscribe( data => {
        console.log(data);
        this.cursoService.listar().subscribe(data => {
          this.cursoService.cursoCambio.next(data);
          this.cursoService.mensajeCambio.next('Se ha modificado el curso');
        });
      });
    }else{
      //insert
      this.cursoService.registrar(this.curso).subscribe( data => {
        console.log(data);
        this.cursoService.listar().subscribe(data => {
          this.cursoService.cursoCambio.next(data);
          this.cursoService.mensajeCambio.next('Se registró el curso');
        });
      });
    }

    this.router.navigate(['curso']);
  }

}
