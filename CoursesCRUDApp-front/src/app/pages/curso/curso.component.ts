import { CursoService } from './../../_service/curso.service';
import { Curso } from './../../_model/curso';
import { Component, OnInit ,ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-curso',
  templateUrl: './curso.component.html',
  styleUrls: ['./curso.component.css']
})
export class CursoComponent implements OnInit {

  displayedColumns = ['idCurso', 'nombre', 'descripcion', 'horas', 'docente', 'vigencia', 'acciones'];
  dataSource: MatTableDataSource<Curso>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private cursoService : CursoService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.cursoService.cursoCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.cursoService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'ATENCIÓN', {
        duration: 2000
      })
    });

    this.cursoService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  applyFilter(valor: string){
    valor = valor.trim();
    valor = valor.toLocaleLowerCase();
    this.dataSource.filter = valor;
  }

  eliminar(curso: Curso): void {
    this.cursoService.eliminar(curso.idCurso).subscribe(data => {
      if (data === 1) {
        this.cursoService.listar().subscribe(data => {
          this.cursoService.cursoCambio.next(data);
          this.cursoService.mensajeCambio.next("Se ha eliminado el curso.");
        });
      }
    });
  }

}
