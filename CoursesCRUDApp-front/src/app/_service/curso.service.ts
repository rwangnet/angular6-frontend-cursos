import { Curso } from './../_model/curso';
import { HOST } from './../_shared/var.constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CursoService {

  url: string = `${HOST}/cursos`
  cursoCambio = new Subject<Curso[]>();
  mensajeCambio = new Subject<string>();

  constructor(private http: HttpClient) { }

  listar(){
    return this.http.get<Curso[]>(this.url)
  }

  listarPorId(id:number){
    return this.http.get<Curso>(`${this.url}/${id}`)
  }

  registrar(cur : Curso){
    return this.http.post<Curso>(this.url, cur)
  }

  modificar(cur : Curso){
    return this.http.put<Curso>(this.url, cur)
  }

  eliminar(id:number){
    return this.http.delete(`${this.url}/${id}`)
  }

}
