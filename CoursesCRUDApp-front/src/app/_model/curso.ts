export class Curso{
  idCurso: number;
  nombre: string;
  descripcion: string;
  cantHoras: number;
  docente: string;
  vigente: boolean;
}
