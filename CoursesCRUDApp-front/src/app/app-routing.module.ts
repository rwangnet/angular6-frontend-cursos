import { CursoEdicionComponent } from './pages/curso/curso-edicion/curso-edicion.component';
import { CursoComponent } from './pages/curso/curso.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path : 'curso', component: CursoComponent, children:[
      { path : 'registrar', component: CursoEdicionComponent },
      { path : 'modificar/:id', component: CursoEdicionComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
